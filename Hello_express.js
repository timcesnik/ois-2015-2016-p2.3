// Za združljivost razvoja na lokalnem računalniku ali v Cloud9 okolju
if (!process.env.PORT) {
  process.env.PORT = 8080;
}

var express = require('express');
var streznik = express();

streznik.get('/', function(zahteva, odgovor) {
  odgovor.send('Lepo pozdravljeni ljubitelji predmeta OIS!');
})

streznik.use(express.static('public'));

streznik.listen(process.env.PORT, function() {
  console.log('Strežnik je pognan!');
})
